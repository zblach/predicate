# predicate

Experimenting with a `predicate` structures in go and the go2 prototype.

A predicate is a function that evaluates a subject and emits a boolean result.