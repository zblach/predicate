package types

import (
	"fmt"
	"predicate/v1/pkg/predicate/strings"
	"predicate/v1/pkg/predicate/types"
	"reflect"
	"testing"
)

func TestMatchSet_Match(t *testing.T) {
	type args struct {
		bag Bag
	}
	tests := []struct {
		name string
		m    MatchSet
		args args
		want types.State
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.m.Match(tt.args.bag); got != tt.want {
				t.Errorf("Match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOptional(t *testing.T) {
	type args struct {
		function types.Function
	}
	tests := []struct {
		name string
		args args
		want fieldMatcher
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Optional(tt.args.function); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Optional() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRequired(t *testing.T) {
	type args struct {
		function types.Function
	}
	tests := []struct {
		name string
		args args
		want fieldMatcher
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Required(tt.args.function); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Required() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_argbag_Get(t *testing.T) {
	type args struct {
		value Field
	}
	tests := []struct {
		name string
		a    argbag
		args args
		want interface{}
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.a.Get(tt.args.value); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_match(t *testing.T) {
	type args struct {
		nilOK bool
		field Field
		f     types.Function
	}
	tests := []struct {
		name string
		args args
		want matcher
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := match(tt.args.nilOK, tt.args.field, tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("match() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMatch(t *testing.T) {
	ms := MatchSet{}

	a := argbag{
		"foo": "fo",
	}

	ms["foo"] = Required(strings.Contains("foo"))
	ms["bar"] = Optional(strings.Contains("bar"))

	fmt.Println(ms.Match(a))
}