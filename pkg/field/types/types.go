package types

import (
	"predicate/v1/pkg/predicate/types"
)

type Field string

const (
	True                  = types.True
	False                 = types.False
	TypeError             = types.Error
	Missing   types.State = "Missing"
)

func match(nilOK bool, field Field, f types.Function) matcher {
	return func(bag Bag) types.State {
		val := bag.Get(field)
		if val == nil {
			if nilOK {
				return True
			}
			return Missing
		}
		return f.Eval(val)
	}
}

type matcher func(bag Bag) types.State
type fieldMatcher func(field Field) matcher

func Required(function types.Function) fieldMatcher {
	return func(field Field) matcher {
		return match(false, field, function)
	}
}

func Optional(function types.Function) fieldMatcher {
	return func(field Field) matcher {
		return match(true, field, function)
	}
}

type MatchSet map[Field]fieldMatcher

func (m *MatchSet) Match(bag Bag) types.State {
	for field, fun := range *m {
		if fun(field)(bag) == False {
			return False
		}
	}
	return True
}

type Bag interface {
	Get(field Field) interface{}
}

type argbag map[Field]interface{}

func (a argbag) Get(value Field) interface{} {
	return (a)[value]
}
