package types

import (
	"fmt"
	"runtime"
	"strings"
	"sync"
)

// Predicate is a function that takes an argument and evaluates it
type Predicate func(subject interface{}) State

type Arg struct {
	name string
	value interface{}
}

type Function interface {
	Eval(subject interface{}) State
	Relabel() Function
	Args() []Arg
}

type fun struct {
	// For naming the function
	name    string
	context []interface{}

	p Predicate
}

func (f fun) Args() []Arg {
	panic("implement me")
}

func getName() string {
	pc, _, _, ok := runtime.Caller(3)
	if !ok {
		return ""
	}
	caller := runtime.FuncForPC(pc)
	if caller == nil {
		panic("no caller")
	}

	return caller.Name()
}

func (f fun) Relabel() Function {
	f.name = getName()
	return f
}

func String(f Function) string {
	if f, ok := f.(fun); ok {
		return flatten(f)
	}

	return ""
}

func flatten(f fun) string {
	strs := make([]string, 0, len(f.context))
	for _, s := range f.context {
		if f, ok := s.(fun); ok {
			strs = append(strs, flatten(f))
		} else {
			strs = append(strs, fmt.Sprintf("%v", s))
		}
	}
	return fmt.Sprintf("%s(%s)", f.name, strings.Join(strs, ","))
}

func (f fun) Eval(subject interface{}) State {
	return f.p(subject)
}

var _ Function = fun{}



func Func(predicate Predicate, argv ...interface{}) fun {
	return getOrNew(getName(), predicate, argv[:])
}

type ctx struct {
	context []interface{}
	p       Predicate
}

var cacheMutex = sync.RWMutex{}
var cache = map[string][]ctx{}

func getOrNew(name string, predicate Predicate, context []interface{}) fun {
	cacheMutex.Lock()
	defer cacheMutex.Unlock()

	if vs, ok := cache[name]; ok {
		Next:
		for _, v := range vs {
			if len(context) != len(v.context) {
				continue
			}
			for i, a := range v.context {
				if _, ok := context[i].(Predicate); ok {
					// recurse
				}
				if context[i] != a {
					continue Next
				}
			}
		}
	}
	return fun{name, context, predicate}
}

type State string

// 'booleanish' return types of Predicates
const (
	True  State = "True"
	False State = "False"
	Error State = "Error"
)

func ToState(b bool) State {
	if b {
		return True
	}
	return False
}