package composite

import (
	"predicate/v1/pkg/predicate/types"
)

// Or composes two or more predicates, evaluates them in order, and returns True if any of them evaluate to True.
func Or(a, b types.Function, more ...types.Function) types.Function {
	ps := append([]types.Function{a, b}, more...)
	return types.Func(func(subject interface{}) types.State {
		hadTypeError := false
		for _, p := range ps {
			switch p.Eval(subject) {
			case types.True:
				return types.True
			case types.Error:
				hadTypeError = true
			}
		}
		if hadTypeError {
			return types.Error
		}
		return types.False
	}, ps)
}

// And composes two or more predicates, evaluates them in order, and returns True if all of them evaluate to True
func And(a, b types.Function, more ...types.Function) types.Function {
	ps := append([]types.Function{a, b}, more...)
	return types.Func(func(subject interface{}) types.State {
		hadTypeError := false
		for _, p := range ps {
			switch p.Eval(subject) {
			case types.False:
				return types.False
			case types.Error:
				hadTypeError = true
			}
		}
		if hadTypeError {
			return types.Error
		}
		return types.True
	}, ps)
}

// Not negates a predicate
func Not(p types.Function) types.Function {
	return types.Func(func(subject interface{}) types.State {
		switch p.Eval(subject) {
		case types.True:
			return types.False
		case types.False:
			return types.True
		}
		return types.Error
	}, p)
}
