package predicate

import (
	"predicate/v1/pkg/predicate/types"
	"reflect"
)

// Equals evaluates whether the target and subject are of equal types and values or not
func Equals(target interface{}) types.Function {
	targType := reflect.TypeOf(target)
	return types.Func(func(subject interface{}) types.State {
		subjType := reflect.TypeOf(subject)
		switch {
		case targType != subjType:
			return types.Error
		case target == subject:
			return types.True
		}
		return types.False
	}, target)
}
