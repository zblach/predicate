package numeric

import (
	"math"
	"predicate/v1/pkg/predicate/composite"
	"predicate/v1/pkg/predicate/types"
	"reflect"
	"strconv"
)

var (
	floatType      = reflect.TypeOf(float64(0))
	stringType     = reflect.TypeOf("")
	floatMinNormal = math.Float64frombits(0x0010000000000000)
)

func toFloat64(val interface{}) (float64, bool) {
	switch f := val.(type) {
	case float64:
		return f, true
	case float32:
		return float64(f), true
	case int64:
		return float64(f), true
	case int32:
		return float64(f), true
	case int16:
		return float64(f), true
	case int8:
		return float64(f), true
	case int:
		return float64(f), true
	case uint64:
		return float64(f), true
	case uint32:
		return float64(f), true
	case uint16:
		return float64(f), true
	case uint8:
		return float64(f), true
	case uint:
		return float64(f), true
	case string:
		if r, err := strconv.ParseFloat(f, 64); err == nil {
			return r, true
		}
		return math.NaN(), false
	}

	v := reflect.ValueOf(val)
	v = reflect.Indirect(v)
	if v.Type().ConvertibleTo(floatType) {
		fv := v.Convert(floatType)
		return fv.Float(), true
	} else if v.Type().ConvertibleTo(stringType) {
		sv := v.Convert(stringType)
		s := sv.String()
		if r, err := strconv.ParseFloat(s, 64); err == nil {
			return r, true
		}
	}
	return math.NaN(), false
}

// LessThan returns True if target > f
func LessThan(target float64) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if f, ok := toFloat64(subject); !ok {
			return types.Error
		} else if target > f {
			return types.True
		}
		return types.False
	}, target)
}

// GreaterThan returns True if target < f
func GreaterThan(target float64) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if f, ok := toFloat64(subject); !ok {
			return types.Error
		} else if target < f {
			return types.True
		}
		return types.False
	}, target)
}

// Epsilon returns whether or not two numbers are approximately (epsilon) equal
func Epsilon(target, epsilon float64) types.Function {
	return types.Func(func(subject interface{}) types.State {
		f, ok := toFloat64(subject)
		if !ok {
			return types.Error
		} else if target == f {
			return types.True
		}
		// https://floating-point-gui.de/errors/comparison/
		absA := math.Abs(target)
		absB := math.Abs(f)
		diff := math.Abs(target - f)

		switch {
		case target == 0, f == 0, absA+absB < floatMinNormal:
			if diff < (epsilon * floatMinNormal) {
				return types.True
			}
			return types.False
		case diff/math.Min(absA+absB, math.MaxFloat64) < epsilon:
			return types.True
		}
		return types.False
	}, target, epsilon)
}

func Equals(target float64) types.Function {
	return Epsilon(target, math.SmallestNonzeroFloat64).Relabel()
}

func LessThanOrEqualTo(target float64) types.Function {
	return composite.Not(GreaterThan(target)).Relabel()
}

func GreaterThanOrEqualTo(target float64) types.Function {
	return composite.Not(LessThan(target)).Relabel()
}

func Between(low, high float64) types.Function {
	return composite.And(GreaterThan(low), LessThan(high)).Relabel()
}
