package strings

import (
	"predicate/v1/pkg/predicate/types"
	"regexp"
	"strings"
)

func Equals(str string) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if s, ok := subject.(string); !ok {
			return types.Error
		} else if s == str {
			return types.True
		}
		return types.False
	}, str)
}

func HasPrefix(prefix string) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if s, ok := subject.(string); !ok {
			return types.Error
		} else if strings.HasPrefix(s, prefix) {
			return types.True
		}
		return types.False
	}, prefix)
}

func HasSuffix(suffix string) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if s, ok := subject.(string); !ok {
			return types.Error
		} else if strings.HasSuffix(s, suffix) {
			return types.True
		}
		return types.False
	}, suffix)
}

func Contains(infix string) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if s, ok := subject.(string); !ok {
			return types.Error
		} else if strings.Contains(s, infix) {
			return types.True
		}
		return types.False
	}, infix)
}

func Matches(regexp regexp.Regexp) types.Function {
	return types.Func(func(subject interface{}) types.State {
		if s, ok := subject.(string); !ok {
			return types.Error
		} else if regexp.MatchString(s) {
			return types.True
		}
		return types.False
	}, regexp)
}
