package predicate

import (
	"predicate/v1/pkg/predicate/composite"
	"predicate/v1/pkg/predicate/numeric"
	"predicate/v1/pkg/predicate/types"
	"testing"
)

func TestEquals(t *testing.T) {
	tests := []struct {
		name      string
		predicate types.Function
		arg       interface{}
		expected  types.State
	}{
		{
			"simple 5",
			Equals(5),
			5,
			types.True,
		},
		{
			"complicated 5",
			composite.Not(composite.Not(Equals(5))),
			5,
			types.True,
		},
		{
			"type error",
			Equals(5),
			"five",
			types.Error,
		},
		{
			"impossible",
			composite.And(numeric.Equals(2), composite.Not(numeric.Equals(2))),
			2,
			types.False,
		},
		{
			"possible",
			composite.Or(composite.Not(Equals(2)), Equals(2)),
			2,
			types.True,
		},
		// Numeric
		{
			"regular equals 2",
			Equals(2),
			2.00000000000000001,
			types.Error,
		},
		{
			"numeric equal 2",
			numeric.Equals(2),
			2.000000000000000001,
			types.True,
		},
		{
			"epsilon equal 2",
			numeric.Epsilon(2.0, 0.5),
			2.3,
			types.True,
		},
		{
			"range",
			numeric.Between(1.0, 3.0),
			2,
			types.True,
		},
		{
			"range low",
			numeric.Between(2.0, 3.0),
			1.0,
			types.False,
		},
		{
			"range high",
			numeric.Between(1.0, 2.0),
			3.0,
			types.False,
		},
		{
			"prefix",
			strings.HasPrefix("pref"),
			"prefix",
			types.True,
		},
		{
			"suffix",
			strings.HasSuffix("fix"),
			"suffix",
			types.True,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			//t.Log(types.String(tt.predicate))
			if got := tt.predicate.Eval(tt.arg); got != tt.expected {
				t.Errorf("For %s, expected %s and got %v", types.String(tt.predicate), tt.expected, got)
			}
		})
	}
}
